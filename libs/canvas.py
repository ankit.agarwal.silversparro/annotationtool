from PyQt4.QtGui import *
from PyQt4.QtCore import *


from shape import Shape
from lib import distance
import radioWidget
import menuWidget

import os
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
import xml.etree.cElementTree as ET
from xml.dom import minidom
from lxml import etree
from modelsXML import SegmentObjectModel, ImageModel

CURSOR_DEFAULT = Qt.ArrowCursor
CURSOR_POINT   = Qt.PointingHandCursor
CURSOR_DRAW    = Qt.CrossCursor
CURSOR_MOVE    = Qt.ClosedHandCursor
CURSOR_GRAB    = Qt.OpenHandCursor

click_flag = True
eraser_flag = False
radioTags_flag = True
shapeIsSelected_flag = False
deSelect_flag = True
acrossFiles_tempShape = []
tempRadioMeaningH = []
tempRadioMeaningV = []
tempRadioMeaningI = []
tempRadioMeaningB = []
acrossFiles_shapeColoured = []
previousLabel = "Default"

#class Canvas(QGLWidget):
class Canvas(QWidget):
    zoomRequest = pyqtSignal(int)
    scrollRequest = pyqtSignal(int, int)
    newShape = pyqtSignal()
    newShape_New = pyqtSignal()
    selectionChanged = pyqtSignal(bool)
    shapeMoved = pyqtSignal()
    drawingPolygon = pyqtSignal(bool)
    CREATE, EDIT = range(2)
    epsilon = 11.0

    def __init__(self, *args, **kwargs):
        super(Canvas, self).__init__(*args, **kwargs)
        # Initialise local state.
        self.mode = self.EDIT
        self.shapes = []
        self.alignments = []
        self.current = None
        self.selectedShape=None # save the selected shape here
        self.selectedShapeCopy=None
        self.filename = ""
        self.XMLRoot_Image = ""
        self.totalShapes=[]
        self.xmldoc = ""
        self.imageModel = None
        self.lineColor = QColor(0, 0, 255)
        self.line = Shape(line_color=self.lineColor)
        self.prevPoint = QPointF()
        self.offsets = QPointF(), QPointF()
        self.scale = 1.0
        self.pixmap = QPixmap()
        self.visible = {}
        self._hideBackround = False
        self.hideBackround = False
        self.hShape = None
        self.hVertex = None
        self._painter = QPainter()
        self._cursor = CURSOR_DEFAULT
        # Menus:
        self.menus = (QMenu(), QMenu())
        # Set widget options.
        self.setMouseTracking(True)
        self.setFocusPolicy(Qt.WheelFocus)

    def enterEvent(self, ev):
        self.overrideCursor(self._cursor)

    def leaveEvent(self, ev):
        self.restoreCursor()

    def focusOutEvent(self, ev):
        self.restoreCursor()

    def isVisible(self, shape):
        return self.visible.get(shape, True)

    def drawing(self):
        return self.mode == self.CREATE

    def editing(self):
        return self.mode == self.EDIT

    def setEraser(self,value=None):
        global eraser_flag
        if value is None:
            return eraser_flag
        if value is True:
            eraser_flag = True
            return eraser_flag
        if value is False:
            eraser_flag = False
            return eraser_flag

    def setEditing(self, value=True):
        self.mode = self.EDIT if value else self.CREATE
        if not value: # Create
            self.unHighlight()
            self.deSelectShape()

    def unHighlight(self):
        if self.hShape:
            self.hShape.highlightClear()
        self.hVertex = self.hShape = None

    def selectedVertex(self):
        return self.hVertex is not None

    def mouseMoveEvent(self, ev):
        """Update line with last point and current coordinates."""
        pos = self.transformPos(ev.posF())

        self.restoreCursor()

        # Polygon drawing.
        if self.drawing():
            self.overrideCursor(CURSOR_DRAW)
            if self.current:
                color = self.lineColor
                if self.outOfPixmap(pos):
                    # Don't allow the user to draw outside the pixmap.
                    # Project the point to the pixmap's edges.
                    pos = self.intersectionPoint(self.current[-1], pos)
                elif len(self.current) > 1 and self.closeEnough(pos, self.current[0]):
                    # Attract line to starting point and colorise to alert the user:
                    pos = self.current[0]
                    color = self.current.line_color
                    self.overrideCursor(CURSOR_POINT)
                    self.current.highlightVertex(0, Shape.NEAR_VERTEX)
                self.line[1] = pos
                self.line.line_color = color
                self.repaint()
                self.current.highlightClear()
            return

        # Polygon/Vertex moving.
        if Qt.LeftButton & ev.buttons():
            if self.selectedVertex():
                nodes = len(self.imageModel.segments)
                old_xmin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                old_ymin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                old_xmax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                old_ymax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                self.boundedMoveVertex(pos)
                new_xmin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                new_ymin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                new_xmax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                new_ymax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                for index in range(0,(nodes)):
                    if str(self.imageModel.segments[index].xmin) == str(old_xmin) \
                        and str(self.imageModel.segments[index].ymin) == str(old_ymin) \
                            and str(self.imageModel.segments[index].xmax) == str(old_xmax) \
                                and str(self.imageModel.segments[index].ymax) == str(old_ymax):

                        self.imageModel.segments[index].xmin = new_xmin
                        self.imageModel.segments[index].ymin = new_ymin
                        self.imageModel.segments[index].xmax = new_xmax
                        self.imageModel.segments[index].ymax = new_ymax

                self.shapeMoved.emit()
                self.repaint()
            elif self.selectedShape and self.prevPoint:
                self.overrideCursor(CURSOR_MOVE)
                self.boundedMoveShape(self.selectedShape, pos)
                self.shapeMoved.emit()
                self.repaint()
            return

        # Just hovering over the canvas, 2 posibilities:
        # - Highlight shapes
        # - Highlight vertex
        # Update shape/vertex fill and tooltip value accordingly.
        self.setToolTip("Image")
        for shape in reversed([s for s in self.shapes if self.isVisible(s)]):
            # Look for a nearby vertex to highlight. If that fails,
            # check if we happen to be inside a shape.
            index = shape.nearestVertex(pos, self.epsilon)
            #Point Hover
            if index is not None:
                if self.selectedVertex():
                    self.hShape.highlightClear()
                self.hVertex, self.hShape = index, shape
                shape.highlightVertex(index, shape.MOVE_VERTEX)
                self.overrideCursor(CURSOR_POINT)
                self.setToolTip("Click & drag to move point")
                self.setStatusTip(self.toolTip())
                if eraser_flag is True:
                    acrossFiles_tempShape.append(shape)
                    self.shapes.remove(shape)
                self.update()
                break
            #Shape Hover
            elif shape.containsPoint(pos):
                if self.selectedVertex():
                    self.hShape.highlightClear()
                self.hVertex, self.hShape = None, shape
                self.setToolTip("Click & drag to move shape '%s'" % shape.label)
                self.setStatusTip(self.toolTip())
                if eraser_flag is True:
                    acrossFiles_tempShape.append(shape)
                    self.shapes.remove(shape)
                self.overrideCursor(CURSOR_GRAB)
                self.update()
                break
        else: # Nothing found, clear highlights, reset state.
            if self.hShape:
                self.hShape.highlightClear()
                self.update()
            self.hVertex, self.hShape = None, None

    def mousePressEvent(self, ev):
        import labelImg
        global click_flag
        global shapeIsSelected_flag
        global radioTags_flag
        global acrossFiles_shapeColoured
        global deSelect_flag
        global tempRadioMeaningH
        global tempRadioMeaningV
        global tempRadioMeaningI
        global tempRadioMeaningB
        global previousLabel
        click_flag = False
        pos = self.transformPos(ev.posF())
        if menuWidget.menuButtonSelected == "" or menuWidget.menuButtonSelected == "Labelling Off":
            if ev.button() == Qt.LeftButton:
                if self.drawing():
                    if self.current and self.current.reachMaxPoints() is False:
                        initPos = self.current[0]
                        minX = initPos.x()
                        minY = initPos.y()
                        targetPos = self.line[1]
                        maxX = targetPos.x()
                        maxY = targetPos.y()

                        self.current.addPoint(QPointF(maxX, minY))
                        self.current.addPoint(targetPos)
                        self.current.addPoint(QPointF(minX, maxY))
                        self.current.addPoint(initPos)
                        self.line[0] = self.current[-1]
                        if self.current.isClosed():
                            self.finalise()
                    elif not self.outOfPixmap(pos):
                        self.current = Shape()
                        self.current.addPoint(pos)
                        self.line.points = [pos, pos]
                        self.setHiding()
                        self.drawingPolygon.emit(True)
                        self.update()
                else:
                    shapeIsSelected_flag = False
                    self.selectShapePoint(pos)
                    if radioWidget.radioButtonSelected=="" or radioWidget.radioButtonSelected=="Default" and shapeIsSelected_flag == True:
                        radioTags_flag = True
                        previousLabel = radioWidget.radioButtonSelected
                        acrossFiles_shapeColoured = []
                    if radioWidget.radioButtonSelected=="Horizontal" and shapeIsSelected_flag == True:
                        if radioTags_flag == True:
                            self.deSelectShapeAll()
                            self.totalShapes = []
                            self.savethatXml(previousLabel)
                            previousLabel = radioWidget.radioButtonSelected
                            self.selectShapePoint(pos)
                            radioTags_flag = False
                            labelImg.MainWindow().chooseColor1New()
                            labelImg.horizontal_count = labelImg.horizontal_count+1
                        for shape in self.totalShapes:
                            if shape not in tempRadioMeaningH:
                                tempRadioMeaningH.append(shape)
                    if radioWidget.radioButtonSelected=="Vertical" and shapeIsSelected_flag == True:
                        if radioTags_flag == True:
                            self.deSelectShapeAll()
                            self.totalShapes = []
                            self.savethatXml(previousLabel)
                            previousLabel = radioWidget.radioButtonSelected
                            self.selectShapePoint(pos)
                            radioTags_flag = False
                            labelImg.MainWindow().chooseColor1New()
                            labelImg.vertical_count = labelImg.vertical_count+1
                        for shape in self.totalShapes:
                            if shape not in tempRadioMeaningV:
                                tempRadioMeaningV.append(shape)
                    if radioWidget.radioButtonSelected=="Bulk" and shapeIsSelected_flag == True:
                        if radioTags_flag == True:
                            self.deSelectShapeAll()
                            self.totalShapes = []
                            self.savethatXml(previousLabel)
                            previousLabel = radioWidget.radioButtonSelected
                            self.selectShapePoint(pos)
                            radioTags_flag = False
                            labelImg.MainWindow().chooseColor1New()
                            labelImg.bulk_count = labelImg.bulk_count+1
                        for shape in self.totalShapes:
                            if shape not in tempRadioMeaningB:
                                tempRadioMeaningB.append(shape)
                    if radioWidget.radioButtonSelected=="Individual" and shapeIsSelected_flag == True:
                        if radioTags_flag == True:
                            self.deSelectShapeAll()
                            self.totalShapes = []
                            self.savethatXml(previousLabel)
                            previousLabel = radioWidget.radioButtonSelected
                            self.selectShapePoint(pos)
                            radioTags_flag = False
                            labelImg.MainWindow().chooseColor1New()
                            labelImg.individual_count = labelImg.individual_count+1
                        for shape in self.totalShapes:
                            if shape not in tempRadioMeaningI:
                                tempRadioMeaningI.append(shape)
                    self.prevPoint = pos
                    self.repaint()
            elif ev.button() == Qt.RightButton and self.editing():
                deSelect_flag = self.deSelectShapePoint(pos)
                radioTags_flag = True
                self.savethatXml(radioWidget.radioButtonSelected)
                self.prevPoint = pos
                self.repaint()
        if menuWidget.menuButtonSelected == "Labelling On":
            if ev.button() == Qt.LeftButton:
                if self.drawing():
                    if self.current and self.current.reachMaxPoints() is False:
                        initPos = self.current[0]
                        minX = initPos.x()
                        minY = initPos.y()
                        targetPos = self.line[1]
                        maxX = targetPos.x()
                        maxY = targetPos.y()
                        self.current.addPoint(QPointF(maxX, minY))
                        self.current.addPoint(targetPos)
                        self.current.addPoint(QPointF(minX, maxY))
                        self.current.addPoint(initPos)
                        self.line[0] = self.current[-1]
                        if self.current.isClosed():
                            self.finalise()
                    elif not self.outOfPixmap(pos):
                        self.current = Shape()
                        self.current.addPoint(pos)
                        self.line.points = [pos, pos]
                        self.setHiding()
                        self.drawingPolygon.emit(True)
                        self.update()
                else:
                    shapeIsSelected_flag = False
                    self.selectShapePoint(pos)
                    # if shapeIsSelected_flag:
                    #     print "Success"
                    # self.newShape_New.emit()
                    self.prevPoint = pos
                    self.repaint()
            elif ev.button() == Qt.RightButton and self.editing():
                deSelect_flag = self.deSelectShapePoint(pos)
                self.prevPoint = pos
                self.repaint()
        click_flag = True

    def savethatXml(self,label):
        import labelImg
        global tempRadioMeaningI
        global tempRadioMeaningH
        global tempRadioMeaningV
        global tempRadioMeaningB
        if label == "Horizontal":
            count = labelImg.horizontal_count
            updateText = "h"+str(count)
            for shape in tempRadioMeaningH:
                xmin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                ymin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                xmax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                ymax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                self.locatePosn(shape,label,updateText,xmin,ymin,xmax,ymax)
            tempRadioMeaningH =[]
        if label == "Vertical":
            count = labelImg.vertical_count
            updateText ="v"+str(count)
            for shape in tempRadioMeaningV:
                xmin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                ymin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                xmax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                ymax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                self.locatePosn(shape,label,updateText,xmin,ymin,xmax,ymax)
            tempRadioMeaningV = []
        if label == "Bulk":
            count = labelImg.bulk_count
            updateText ="b"+str(count)
            for shape in tempRadioMeaningB:
                xmin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                ymin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                xmax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                ymax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                self.locatePosn(shape,label,updateText,xmin,ymin,xmax,ymax)
            tempRadioMeaningB = []
        if label == "Individual":
            count = labelImg.individual_count
            updateText ="i"+str(count)
            for shape in tempRadioMeaningI:
                xmin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                ymin = int(float(str(shape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                xmax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                ymax = int(float(str(shape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                self.locatePosn(shape,label,updateText,xmin,ymin,xmax,ymax)
            tempRadioMeaningI = []

    def locatePosn(self,shape,label,updateText,Compare_2_xmin,Compare_2_ymin,Compare_2_xmax,Compare_2_ymax):

        if self.filename is not None:
            objectlist = self.imageModel.segments

            for check, object1 in enumerate(objectlist):
                Compare_1_xmin = int(str(object1.xmin))
                Compare_1_ymin = int(str(object1.ymin))
                Compare_1_xmax = int(str(object1.xmax))
                Compare_1_ymax = int(str(object1.ymax))

                if (int(Compare_2_xmax) == Compare_1_xmax) and (int(Compare_2_xmin) == Compare_1_xmin) and (int(Compare_2_ymax) == Compare_1_ymax) and (int(Compare_2_ymin) == Compare_1_ymin):
                    # root = xmldoc.getElementsByTagName('annotation')
                    if label == "Horizontal":
                        self.imageModel.segments[check].horizontal = updateText
                    if label == "Vertical":
                        self.imageModel.segments[check].vertical = updateText
                    if label == "Individual":
                        self.imageModel.segments[check].individual = updateText
                    if label == "Bulk":
                        self.imageModel.segments[check].bulk = updateText

                    # file_handle = open(fullname,"wb")
                    # root[0].writexml(file_handle)
                    # file_handle.close()

    def mouseReleaseEvent(self, ev):
        if ev.button() == Qt.RightButton:
            if menuWidget.menuButtonSelected == "" or menuWidget.menuButtonSelected == "Labelling Off":
                if deSelect_flag is False:
                    self.deSelectShapeAll()
                    self.totalShapes = []
                    menu = self.menus[bool(self.selectedShapeCopy)]
                    self.restoreCursor()
                    if not menu.exec_(self.mapToGlobal(ev.pos()))\
                       and self.selectedShapeCopy:
                        # Cancel the move by deleting the shadow copy.
                        self.selectedShapeCopy = None
                        self.repaint()
            elif menuWidget.menuButtonSelected == "Labelling On":
                if deSelect_flag is False:
                    self.newShape_New.emit()
                    self.deSelectShapeAll()
                    self.totalShapes = []
                    self.restoreCursor()
                    if self.selectedShapeCopy:
                        # Cancel the move by deleting the shadow copy.
                        self.selectedShapeCopy = None
                        self.repaint()

    def hideBackroundShapes(self, value):
        self.hideBackround = value
        if self.selectedShape:
            # Only hide other shapes if there is a current selection.
            # Otherwise the user will not be able to select a shape.
            self.setHiding(True)
            self.repaint()

    def setHiding(self, enable=True):
        self._hideBackround = self.hideBackround if enable else False

    def canCloseShape(self):
        return self.drawing() and self.current and len(self.current) > 2

    def mouseDoubleClickEvent(self, ev):
        # We need at least 4 points here, since the mousePress handler
        # adds an extra one before this handler is called.
        if self.canCloseShape() and len(self.current) > 3:
            self.current.popPoint()
            self.finalise()

    def selectShape(self, shape):
        if (click_flag == True):
            self.deSelectShapeAll()
            self.totalShapes = []
            shape.selected = True
            self.selectedShape = shape
            self.totalShapes.append(shape)
            self.setHiding()
            self.selectionChanged.emit(True)
            self.update()

    def deSelectShapePoint(self,point):
        self.selectShapePoint(point)
        self.deSelectShape()
        for shape in reversed(self.shapes):
            if self.isVisible(shape) and shape.containsPoint(point):
                if shape in self.totalShapes:
                    self.totalShapes.remove(shape)
                    self.setHiding()
                    self.selectionChanged.emit(True)
                    return True
        return False

    def selectShapePoint(self, point):
        """Select the first shape created which contains this point."""
        global acrossFiles_shapeColoured
        global shapeIsSelected_flag
        if self.selectedVertex(): # A vertex is marked for selection.
            index, shape = self.hVertex, self.hShape
            shape.highlightVertex(index, shape.MOVE_VERTEX)
            return
        for shape in reversed(self.shapes):
            if self.isVisible(shape) and shape.containsPoint(point):
                shape.selected = True
                self.selectedShape = shape
                if shape not in self.totalShapes:
                    self.totalShapes.append(shape)
                    if (shape not in acrossFiles_shapeColoured):
                        acrossFiles_shapeColoured.append(shape)
                    shapeIsSelected_flag = True
                elif shape:
                    if shape in self.totalShapes:
                        self.totalShapes.remove(shape)
                        if shape in acrossFiles_shapeColoured:
                            acrossFiles_shapeColoured.remove(shape)
                        if radioWidget.radioButtonSelected == "Horizontal":
                            tempRadioMeaningH.remove(shape)
                        if radioWidget.radioButtonSelected == "Vertical":
                            tempRadioMeaningV.remove(shape)
                        if radioWidget.radioButtonSelected == "Individual":
                            tempRadioMeaningI.remove(shape)
                        if radioWidget.radioButtonSelected == "Bulk":
                            tempRadioMeaningB.remove(shape)
                self.calculateOffsets(shape, point)
                self.setHiding()
                self.selectionChanged.emit(True)
                return

    def calculateOffsets(self, shape, point):
        rect = shape.boundingRect()
        x1 = rect.x() - point.x()
        y1 = rect.y() - point.y()
        x2 = (rect.x() + rect.width()) - point.x()
        y2 = (rect.y() + rect.height()) - point.y()
        self.offsets = QPointF(x1, y1), QPointF(x2, y2)

    def boundedMoveVertex(self, pos):
        index, shape = self.hVertex, self.hShape
        point = shape[index]
        if self.outOfPixmap(pos):
            pos = self.intersectionPoint(point, pos)

        shiftPos = pos - point
        if isinstance(shape.label,str):
            shape.moveVertexByNew(index, shiftPos)
        else:
            shape.moveVertexBy(index, shiftPos)
            lindex = (index + 1) % 4
            rindex = (index + 3) % 4
            lshift = None
            rshift = None
            if index % 2 == 0:
                rshift = QPointF(shiftPos.x(), 0)
                lshift = QPointF(0, shiftPos.y())
            else:
                lshift = QPointF(shiftPos.x(), 0)
                rshift = QPointF(0, shiftPos.y())
            shape.moveVertexBy(rindex, rshift)
            shape.moveVertexBy(lindex, lshift)

    def boundedMoveShape(self, shape, pos):
        if self.outOfPixmap(pos):
            return False # No need to move
        o1 = pos + self.offsets[0]
        if self.outOfPixmap(o1):
            pos -= QPointF(min(0, o1.x()), min(0, o1.y()))
        o2 = pos + self.offsets[1]
        if self.outOfPixmap(o2):
            pos += QPointF(min(0, self.pixmap.width() - o2.x()),
                           min(0, self.pixmap.height()- o2.y()))
        # The next line tracks the new position of the cursor
        # relative to the shape, but also results in making it
        # a bit "shaky" when nearing the border and allows it to
        # go outside of the shape's area for some reason. XXX
        #self.calculateOffsets(self.selectedShape, pos)
        dp = pos - self.prevPoint
        if dp:
            if self.hShape is not None:
                nodes = len(self.imageModel.segments)
                old_xmin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                old_ymin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                old_xmax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                old_ymax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                shape.moveBy(dp)
                new_xmin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[0]))
                new_ymin = int(float(str(self.hShape.points[0]).split("(")[1].split(")")[0].split(",")[1]))
                new_xmax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[0]))
                new_ymax = int(float(str(self.hShape.points[2]).split("(")[1].split(")")[0].split(",")[1]))
                for index in range(0,(nodes)):
                    if(str(self.imageModel.segments[index].xmin)==str(old_xmin)) \
                        and (str(self.imageModel.segments[index].ymin)==str(old_ymin)) \
                            and (str(self.imageModel.segments[index].xmax)==str(old_xmax)) \
                                and (str(self.imageModel.segments[index].ymax)==str(old_ymax)):

                        self.imageModel.segments[index].xmin = new_xmin
                        self.imageModel.segments[index].ymin = new_ymin
                        self.imageModel.segments[index].xmax = new_xmax
                        self.imageModel.segments[index].ymax = new_ymax

            self.prevPoint = pos
            return True
        return False

    def deSelectShape(self):
        if self.selectedShape:
            # if self.selectedShape in self.totalShapes:
            #     self.totalShapes.remove(self.selectedShape)
            self.selectedShape.selected = False
            self.selectedShape = None
            self.setHiding(False)
            self.selectionChanged.emit(False)
            self.update()

    def deSelectShapeAll(self):
        for shape in self.totalShapes:
            self.selectedShape = shape
            self.selectedShape.selected = False
            self.selectedShape = None
            self.setHiding(False)
            self.selectionChanged.emit(False)
            self.update()

    def deleteSelected(self):
        tempShape = []
        for shape in self.totalShapes:
        #Disabled
        # if self.selectedShape:
            # shape = self.selectedShape
            tempShape.append(shape)
            self.shapes.remove(shape)
            #Disabled
            # self.selectedShape = None
            # self.update()
        self.totalShapes = []
        return tempShape

    def copySelectedShape(self):
        if self.selectedShape:
            shape = self.selectedShape.copy()
            self.deSelectShape()
            self.shapes.append(shape)
            shape.selected = True
            self.selectedShape = shape
            self.boundedShiftShape(shape)
            return shape

    def boundedShiftShape(self, shape):
        # Try to move in one direction, and if it fails in another.
        # Give up if both fail.
        point = shape[0]
        offset = QPointF(2.0, 2.0)
        self.calculateOffsets(shape, point)
        self.prevPoint = point
        if not self.boundedMoveShape(shape, point - offset):
            self.boundedMoveShape(shape, point + offset)

    def paintEvent(self, event):
        if not self.pixmap:
            return super(Canvas, self).paintEvent(event)

        p = self._painter
        p.begin(self)
        p.setRenderHint(QPainter.Antialiasing)
        p.setRenderHint(QPainter.HighQualityAntialiasing)
        p.setRenderHint(QPainter.SmoothPixmapTransform)

        p.scale(self.scale, self.scale)
        p.translate(self.offsetToCenter())

        p.drawPixmap(0, 0, self.pixmap)
        Shape.scale = self.scale
        for shape in self.shapes:
            if (shape.selected or not self._hideBackround) and self.isVisible(shape):
                shape.fill = shape.selected or shape == self.hShape
                if shape in self.totalShapes:
                    shape.paint_red(p)
                elif shape in acrossFiles_shapeColoured:
                    shape.paint_blue(p)
                else:
                    shape.paint(p)
        if self.current:
            self.current.paint(p)
            self.line.paint(p)
        if self.selectedShapeCopy:
            self.selectedShapeCopy.paint(p)

        # Paint rect
        if self.current is not None and len(self.line) == 2:
            leftTop = self.line[0]
            rightBottom = self.line[1]
            rectWidth = rightBottom.x() - leftTop.x()
            rectHeight = rightBottom.y() - leftTop.y()
            color = QColor(0, 220, 0)
            p.setPen(color)
            brush = QBrush(Qt.BDiagPattern)
            p.setBrush(brush)
            p.drawRect(leftTop.x(), leftTop.y(), rectWidth, rectHeight)

        p.end()

    def transformPos(self, point):
        """Convert from widget-logical coordinates to painter-logical coordinates."""
        return point / self.scale - self.offsetToCenter()

    def offsetToCenter(self):
        s = self.scale
        area = super(Canvas, self).size()
        w, h = self.pixmap.width() * s, self.pixmap.height() * s
        aw, ah = area.width(), area.height()
        x = (aw-w)/(2*s) if aw > w else 0
        y = (ah-h)/(2*s) if ah > h else 0
        return QPointF(x, y)

    def outOfPixmap(self, p):
        w, h = self.pixmap.width(), self.pixmap.height()
        return not (0 <= p.x() <= w and 0 <= p.y() <= h)

    def finalise(self):
        assert self.current
        self.current.close()
        self.shapes.append(self.current)
        self.current = None
        self.setHiding(False)
        self.newShape.emit()
        self.update()

        nametext = str(self.shapes[-1].label)

        xmintext = str(int(float(str(self.shapes[-1].points[0]).split("(")[1].split(")")[0].split(",")[0])))

        ymintext = str(int(float(str(self.shapes[-1].points[0]).split("(")[1].split(")")[0].split(",")[1])))

        xmaxtext = str(int(float(str(self.shapes[-1].points[2]).split("(")[1].split(")")[0].split(",")[0])))

        ymaxtext = str(int(float(str(self.shapes[-1].points[2]).split("(")[1].split(")")[0].split(",")[1])))


        objectInformation = SegmentObjectModel()

        objectInformation.setname(nametext)
        objectInformation.setpose("Unspecified")
        objectInformation.settruncated("0")
        objectInformation.setdifficult("0")
        objectInformation.setxmin(xmintext)
        objectInformation.setymin(ymintext)
        objectInformation.setxmax(xmaxtext)
        objectInformation.setymax(ymaxtext)
        objectInformation.sethorizontal(" ")
        objectInformation.setvertical(" ")
        objectInformation.setindividual(" ")
        objectInformation.setbulk(" ")
        self.imageModel.pushSegment(objectInformation)

    def closeEnough(self, p1, p2):
        #d = distance(p1 - p2)
        #m = (p1-p2).manhattanLength()
        #print "d %.2f, m %d, %.2f" % (d, m, d - m)
        return distance(p1 - p2) < self.epsilon

    def intersectionPoint(self, p1, p2):
        # Cycle through each image edge in clockwise fashion,
        # and find the one intersecting the current line segment.
        # http://paulbourke.net/geometry/lineline2d/
        size = self.pixmap.size()
        points = [(0,0),
                  (size.width(), 0),
                  (size.width(), size.height()),
                  (0, size.height())]
        x1, y1 = p1.x(), p1.y()
        x2, y2 = p2.x(), p2.y()
        d, i, (x, y) = min(self.intersectingEdges((x1, y1), (x2, y2), points))
        x3, y3 = points[i]
        x4, y4 = points[(i+1)%4]
        if (x, y) == (x1, y1):
            # Handle cases where previous point is on one of the edges.
            if x3 == x4:
                return QPointF(x3, min(max(0, y2), max(y3, y4)))
            else: # y3 == y4
                return QPointF(min(max(0, x2), max(x3, x4)), y3)
        return QPointF(x, y)

    def intersectingEdges(self, (x1, y1), (x2, y2), points):
        """For each edge formed by `points', yield the intersection
        with the line segment `(x1,y1) - (x2,y2)`, if it exists.
        Also return the distance of `(x2,y2)' to the middle of the
        edge along with its index, so that the one closest can be chosen."""
        for i in xrange(4):
            x3, y3 = points[i]
            x4, y4 = points[(i+1) % 4]
            denom = (y4-y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
            nua = (x4-x3) * (y1-y3) - (y4-y3) * (x1-x3)
            nub = (x2-x1) * (y1-y3) - (y2-y1) * (x1-x3)
            if denom == 0:
                # This covers two cases:
                #   nua == nub == 0: Coincident
                #   otherwise: Parallel
                continue
            ua, ub = nua / denom, nub / denom
            if 0 <= ua <= 1 and 0 <= ub <= 1:
                x = x1 + ua * (x2 - x1)
                y = y1 + ua * (y2 - y1)
                m = QPointF((x3 + x4)/2, (y3 + y4)/2)
                d = distance(m - QPointF(x2, y2))
                yield d, i, (x, y)

    # These two, along with a call to adjustSize are required for the
    # scroll area.
    def sizeHint(self):
        return self.minimumSizeHint()

    def minimumSizeHint(self):
        if self.pixmap:
            return self.scale * self.pixmap.size()
        return super(Canvas, self).minimumSizeHint()

    def wheelEvent(self, ev):
        if ev.orientation() == Qt.Vertical:
            mods = ev.modifiers()
            if Qt.ControlModifier == int(mods):
                self.zoomRequest.emit(ev.delta())
            else:
                self.scrollRequest.emit(ev.delta(),
                        Qt.Horizontal if (Qt.ShiftModifier == int(mods))\
                                      else Qt.Vertical)
        else:
            self.scrollRequest.emit(ev.delta(), Qt.Horizontal)
        ev.accept()

    def keyPressEvent(self, ev):
        key = ev.key()
        if key == Qt.Key_Escape and self.current:
            print 'ESC press'
            self.current = None
            self.drawingPolygon.emit(False)
            self.update()
        elif key == Qt.Key_Return and self.canCloseShape():
            self.finalise()

    def setLastLabel(self, text):
        assert text
        self.shapes[-1].label = text
        return self.shapes[-1]

    def undoLastLine(self):
        assert self.shapes
        self.current = self.shapes.pop()
        self.current.setOpen()
        self.line.points = [self.current[-1], self.current[0]]
        self.drawingPolygon.emit(True)

    def resetAllLines(self):
        import menuWidget
        if menuWidget.menuButtonSelected=="Labelling Off":
            assert self.shapes
            self.current = self.shapes.pop()
            self.current.setOpen()
            self.line.points = [self.current[-1], self.current[0]]
            self.drawingPolygon.emit(True)
            self.current = None
            self.drawingPolygon.emit(False)
            self.update()

    def loadPixmap(self, pixmap):
        self.pixmap = pixmap
        self.shapes = []
        self.repaint()

    def loadShapes(self, shapes):
        self.shapes = list(shapes)
        self.current = None
        self.repaint()

    def setShapeVisible(self, shape, value):
        self.visible[shape] = value
        self.repaint()

    def overrideCursor(self, cursor):
        self.restoreCursor()
        self._cursor = cursor
        QApplication.setOverrideCursor(cursor)

    def restoreCursor(self):
        QApplication.restoreOverrideCursor()

    def resetState(self):
        self.restoreCursor()
        self.pixmap = None
        self.update()

