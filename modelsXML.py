class ImageModel():
    def __init__(self, *args, **kwargs):
        self.folder = ""
        self.filename = ""
        self.path = ""
        self.database = ""
        self.imgWidth = ""
        self.imgHeight = ""
        self.imgDepth = ""
        self.segmented = ""
        self.segments = []
    def getfolder(self):
        return self.folder
    def setfolder(self, folder):
        self.folder = folder
    def getfilename(self):
        return self.filename
    def setfilename(self, filename):
        self.filename = filename
    def getpath(self):
        return self.path
    def setpath(self, path):
        self.path = path
    def getdatabase(self):
        return self.database
    def setdatabase(self, database):
        self.database = database
    def getimgWidth(self):
        return self.imgWidth
    def setimgWidth(self, imgWidth):
        self.imgWidth = imgWidth
    def getimgHeight(self):
        return self.imgHeight
    def setimgHeight(self, imgHeight):
        self.imgHeight = imgHeight
    def getimgDepth(self):
        return self.imgDepth
    def setimgDepth(self, imgDepth):
        self.imgDepth = imgDepth
    def getsegmented(self):
        return self.segmented
    def setsegmented(self, segmented):
        self.segmented = segmented
    def pushSegment(self,segmentObject):
        self.segments.append(segmentObject)
    def deleteSegment(self,segmentObject):
        self.segments.remove(segmentObject)

class SegmentObjectModel():
    def __init__(self, *args, **kwargs):
        self.name = ""
        self.pose = ""
        self.truncated = ""
        self.difficult = ""
        self.xmin = ""
        self.ymin = ""
        self.xmax = ""
        self.ymax = ""
        self.horizontal = ""
        self.vertical = ""
        self.individual = ""
        self.bulk = ""
        self.label = ""

    def getname(self):
        return self.name
    def setname(self, name):
        self.name = name
    def getpose(self):
        return self.pose
    def setpose(self, pose):
        self.pose = pose
    def gettruncated(self):
        return self.truncated
    def settruncated(self, truncated):
        self.truncated = truncated
    def getdifficult(self):
        return self.difficult
    def setdifficult(self, difficult):
        self.difficult = difficult
    def getxmin(self):
        return self.xmin
    def setxmin(self, xmin):
        self.xmin = xmin
    def getymin(self):
        return self.ymin
    def setymin(self, ymin):
        self.ymin = ymin
    def getxmax(self):
        return self.xmax
    def setxmax(self, xmax):
        self.xmax = xmax
    def getymax(self):
        return self.ymax
    def setymax(self, ymax):
        self.ymax = ymax
    def gethorizontal(self):
        return self.horizontal
    def sethorizontal(self, horizontal):
        self.horizontal = horizontal
    def getvertical(self):
        return self.vertical
    def setvertical(self, vertical):
        self.vertical = vertical
    def getindividual(self):
        return self.individual
    def setindividual(self, individual):
        self.individual = individual
    def getbulk(self):
        return self.bulk
    def setbulk(self, bulk):
        self.bulk = bulk
    def getlabel(self):
        return self.label
    def setlabel(self, label):
        self.label = label
