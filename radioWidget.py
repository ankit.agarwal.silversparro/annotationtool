import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

radioButtonSelected = ""

class RadioWidget(QWidget):

   def __init__(self, parent = None):
      super(RadioWidget, self).__init__(parent)

      #Hide this for getting some default value.
      layout = QHBoxLayout()
      self.b1 = QRadioButton("Default")
      self.b1.setChecked(True)
      self.b1.toggled.connect(lambda:self.btnstate(self.b1))
      self.b1.setHidden(True)
      layout.addWidget(self.b1)

      self.b2 = QRadioButton("Horizontal")
      self.b2.toggled.connect(lambda:self.btnstate(self.b2))
      layout.addWidget(self.b2)

      self.b3 = QRadioButton("Vertical")
      self.b3.toggled.connect(lambda:self.btnstate(self.b3))
      layout.addWidget(self.b3)

      self.b4 = QRadioButton("Bulk")
      self.b4.toggled.connect(lambda:self.btnstate(self.b4))
      layout.addWidget(self.b4)

      self.b5 = QRadioButton("Individual")
      self.b5.toggled.connect(lambda:self.btnstate(self.b5))
      layout.addWidget(self.b5)

      self.setLayout(layout)
      self.setWindowTitle("RadioButton")

   def btnstate(self,b):
      global radioButtonSelected
      global radioButtonSavingLabel

      if b.text() == "Default":
         import canvas
         import labelImg
         if b.isChecked() == True:
            print b.text()+" is selected"
            radioButtonSelected = b.text()
         else:
            ### DEFAULT ACTION ON SETUP ###
            labelImg.horizontal_count = 0
            labelImg.vertical_count = 0
            labelImg.individual_count = 0
            labelImg.bulk_count = 0
            canvas.tempRadioMeaningH = []
            canvas.tempRadioMeaningV = []
            canvas.tempRadioMeaningB = []
            canvas.tempRadioMeaningI = []
            canvas.previouslabel = "Default"
            print b.text()+" is deselected"


      if b.text() == "Horizontal":
         import canvas
         if b.isChecked() == True:
            radioButtonSelected = b.text()
            canvas.radioTags_flag = True
            print b.text()+" is selected"
         else:
            print b.text()+" is deselected"

      if b.text() == "Vertical":
         import canvas
         if b.isChecked() == True:
            radioButtonSelected = b.text()
            canvas.radioTags_flag = True
            print b.text()+" is selected"
         else:
            print b.text()+" is deselected"

      if b.text() == "Bulk":
         import canvas
         if b.isChecked() == True:
            canvas.radioTags_flag = True
            radioButtonSelected = b.text()
            print b.text()+" is selected"
         else:
            print b.text()+" is deselected"

      if b.text() == "Individual":
         import canvas
         if b.isChecked() == True:
            canvas.radioTags_flag = True
            radioButtonSelected = b.text()
            print b.text()+" is selected"
         else:
            print b.text()+" is deselected"

def minimumSizeHint(self):
     height = super(RadioWidget, self).minimumSizeHint().height()
     fm = QFontMetrics(self.font())
     width = fm.width(str(self.maximum()))
     return QSize(width, height)