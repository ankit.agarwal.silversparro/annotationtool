import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

menuButtonSelected = ""

class MenuWidget(QWidget):

   def __init__(self, parent = None):
      super(MenuWidget, self).__init__(parent)

      #Hide this for getting some default value.
      layout = QHBoxLayout()
      self.b1 = QRadioButton("Labelling Off")
      self.b1.setChecked(True)
      self.b1.toggled.connect(lambda:self.btnstate(self.b1))
      layout.addWidget(self.b1)

      self.b2 = QRadioButton("Labelling On")
      self.b2.toggled.connect(lambda:self.btnstate(self.b2))
      layout.addWidget(self.b2)

      self.setLayout(layout)
      self.setWindowTitle("Labelling Button")

   def btnstate(self,b):

      global menuButtonSelected

      if b.text() == "Labelling Off":
         if b.isChecked() == True:
            menuButtonSelected = b.text()
            print b.text()+" is selected"
         else:
            import canvas
            canvas.acrossFiles_shapeColoured = []
            print b.text()+" is deselected"

      if b.text() == "Labelling On":
         if b.isChecked() == True:
            menuButtonSelected = b.text()
            print b.text()+" is selected"
         else:
            import canvas
            canvas.acrossFiles_shapeColoured = []
            print b.text()+" is deselected"

def minimumSizeHint(self):
     height = super(MenuWidget, self).minimumSizeHint().height()
     fm = QFontMetrics(self.font())
     width = fm.width(str(self.maximum()))
     return QSize(width, height)